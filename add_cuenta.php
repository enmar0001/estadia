<?php 
include('conexion.php');
include("librerias/template_er.php");
webCabezal("Coordinadores");

$id=$_SESSION['idServidor'];
$u_curp = $_GET['ucurp'];

$sqlDatos="SELECT * FROM servidores_det_lab where iIdServidor='$id'";
$datos=mysql_query($sqlDatos,$conexion);	
$row=mysql_fetch_array($datos);
$IdDepen=$row['iIdDependencia'];

$queryDepen="SELECT * FROM cat_dependencia";
$depen=mysql_query($queryDepen,$conexion);

$queryPuesto="SELECT * FROM cat_er_puestos";
$puesto=mysql_query($queryPuesto,$conexion);



?>
<!-- calendario/css/jquery-ui-1.7.2.custom.css -->
<link href="../script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="script/jscalendar-0.9.6/calendar.js"></script>
<script type="text/javascript" src="script/calendario.js"></script>
<script type="text/javascript" src="script/validacampos.js"></script>
<script type="text/javascript" src="deptos_add_cuentas.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script> 	
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="calendario/css/jquery-ui-1.7.2.custom.css"/>
<script type="text/javascript">
jQuery(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
        'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['es']);
});    

</script>
<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
<link href="script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="captcha/styles.css" rel="stylesheet" type="text/css" />
<!--EMPIEZA CONTENIDO-->
<body onLoad="deptos(this.value)">
<div align="center">

  <table width="950" border="0"  align="center" >
    <tr>
      <td rowspan="2" valign="bottom"></td>
      <td width="70" align="center"><a href="principal_ce.php"><img src="iconos/i_volver.png" alt="" width="65" height="61" border="0" /></a><a href="javascript:cargaPag('http://srvsweb.contraloria.yucatan.gob.mx:8081/scripts/cgiip.exe/WService=sidepaweb/Consulta.r?usuario=<?php echo $usuario; ?>&clave=<?php echo $pass; ?>')"></a></td>
      <td width="71" align="center"><a href="salir.php"><img src="iconos/n_exit.png" alt="" width="65" height="65" border="0" /></a></td>
    </tr>
    <tr>
      <td align="center">regresar</td>
      <td align="center">Salir</td>
    </tr>
  </table>
  
  <table width="778" border="0"  align="center">
      <tr>
        <td width="772" class="sitios_header"><p>Paso No. 1 :</p>
        <p>Captura de Datos Personales</p></td>
      </tr>
  
  
  
  
  
<form  id="cuentas" name="cuentas" method="post" action="agrega_cuenta.php">
<input type="hidden" id="dep" value="<?php echo $_SESSION['iIdDependencia']; ?>"/>  
  <table width="950" border="0"  align="center" >

    <tr><td colspan="2" class="fila_subtabla_ci">Datos Personales</td></tr>

<tr><td>

<table width="950" border="0" align="center"> 
    <tr>
  <td width="121">CURP: </td>
    <td width="819"> <input type="text" name="curp" id="curp"  value="<?php echo $u_curp ?>"size="60" maxlength="18" onKeyUp="javascript:this.value=this.value.toUpperCase();" onBlur="valida_curp_cuenta()"  /></td>
  </tr>
    <tr>
    <td width="121">Nombre(s): </td> 
    <td width="819"><input type="text" id="nombreServ" size="40" onKeyUp="javascript:this.value=this.value.toUpperCase();" name="nombreServ" /></td>
  </tr>
      <tr>
    <td width="121">Apellido Paterno: </td>
    <td width="819"><input type="text" id="apellidoP" size="40" onKeyUp="javascript:this.value=this.value.toUpperCase();" name="apellidoP"   /></td>
  </tr>
        <tr>
    <td width="121">Apellido Materno: </td>
    <td width="819"><input type="text" id="apellidoM" size="40" onKeyUp="javascript:this.value=this.value.toUpperCase();" name="apellidoM"   /></td>
  </tr>
    <tr>
  <td width="121">Fecha Alta: </td>
    <td width="819"> <input type="text" name="fecha" id="fecha"   /><span style="color:#999999;">Ejemplo: <?php echo date('Y-m-d')?></span></td>
  </tr>
  <td width="121">Login: </td>
    <td width="819"> <input type="text" name="login" id="login"  /></td>
  </tr>
  <tr>
   <td width="121">Contrase&ntilde;a: </td>
    <td width="819"> <input type="password" name="pass" id="pass" /></td>
  </tr>
  <tr>
   <td width="121">Repetir Contrase&ntilde;a: </td>
    <td width="819"> <input type="password" name="reppass" id="reppass" /></td>
  </tr>
  </table>
</td></tr>
<tr>
  <td class="maxima"> <label class="hidden"> 
   Datos Laborales</label></td></tr>
<tr><td>
<table width="950" border="0" align="center">
    <tr>
    <td width="102"><label class="hidden">Dependecia/Entidad</label> </td>
    <td align="left"><label class="hidden">  <?php
	echo "<select disabled class='select' id='add_depen' name='add_depen'>";
   
    echo $DepenExist ="SELECT * FROM cat_dependencia WHERE nIdDepen='$IdDepen'";
   
    if ($IdDepen!='N'){
		$query1 =mysql_query($DepenExist);
		while ($rowDepen =mysql_fetch_array($query1 )){
			$DepnNombre =$rowDepen['Dep_Nombre'];
		}
		echo "<option value=$IdDepen> $DepnNombre </option>";
	}else{
		echo"<option value='N'>Seleccione dependencia</option>";}
		 while ($fila = mysql_fetch_array($depen)){
		echo'<option value="'.$fila['nIdDepen'].'"> '.$fila['Dep_Nombre'].' </option>';
		 }
     		echo"</select>";
    ?>
    </label></td>
  </tr>
  <td width="102"><label class="hidden">Departamento</label> </td>
  <td><label class="hidden">
  <div id="agrega_deptos"></div>
  </label>
  </td>
  <tr>
   <td width="102"><label class="hidden">puesto</label> </td>
    <td align="left"><label class="hidden"><select class='select' name='idPuesto' id='idPuesto' >
             <option value='N'>Seleccione Puesto</option>";}
		<?php while ($fila = mysql_fetch_array($puesto)){?>
		<option value="<?php echo $fila['idPuesto']?>"> <?php echo $fila['Puesto_Nombre']?> </option>';
		<?php }?>
     		</select>
            </label>
    </td>
  </tr>
  
   <!--<tr>
   <td width="102">Acciones <br />
    </td>
    <td width="838">
            <input name="idTipo" type="radio" value="1"  >Entrega-Recepcion
    				<br>
                    <input name="idTipo"  type="radio" value="2"  >Declaracion Patrimonial
                    <br>
                    <input name="idTipo"  type="radio" value="3"  >Ambos
                        
    </td>
  </tr>-->
  <tr>
  <td></td>
  <td><input type="button" name="Submit" value="" class="boton_save" onClick="valida_addserv()"/>
    Guardar Registro</td>
  </tr>
</table>
</table>


</form>
</td></tr></table>
</div>
<div class="error" align="center" ><p><?php 
	    if (isset($_GET['error'])=='si')
	    {
          echo 'Ya hay un usuario registrado con esta CURP';
	    }?></p>
	  </div>

<div class="error2" align="center" ><p><?php 
      if (isset($_GET['errorpass'])=='si')
      {
          echo 'Las contraseñas no coinciden. Favor de verificar.';
      }?></p>
    </div>



</body>
<!--TERMINA CONTENIDO-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11177675-1");
pageTracker._trackPageview();
} catch(err) {}
</script>




<?php webFooter(); ?>