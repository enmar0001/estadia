<?php session_start();
include("librerias/template_login.php");
webCabezal("Error : SCG");
?>

<script type="text/javascript" src="js/validaforms.js"></script>
<script type="text/javascript" src="script/validacampos.js"></script>
<script type="text/javascript" src="funcion_listado_juridico.js"></script>
<script type="text/javascript" src="script/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="script/datatables/tablas.js"></script>

<link href="../script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
<link href="script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="captcha/styles.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.foundation.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />


<style type="text/css">
<!--
.Estilo1 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>

<table width="662" height="360" border="0" align="center">
  <tr>
    <th height="58" colspan="2" class="title" scope="col">Ha ocurrido un error.</th>
  </tr>
  <tr>
    <td height="37" colspan="2" class="sitios_header">La dirección no se encuentra o no tiene los permisos para ejecutar ésta acción. Favor de comunicarse con el administrador.</td>
  </tr>
</table>



<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11177675-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

<?php webFooter(); ?>