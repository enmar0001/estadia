<?php 
include('conexion.php');
include("librerias/template_er.php");

candado_ec();
webCabezal("Sistema de Gestion para Sujetos Obligados");

$id=$_SESSION['idServidor'];
$idServ= $_GET['id'];

$dat_serv="SELECT servidores.iIdServidor, servidores.cServidorNombre, servidores.cServidorApellidoP, 
servidores.cLogin, servidores.cPassword, servidores.cServidorApellidoM, servidores.cCURP, servidores.tObservacion, 
servidores.dFechaAlta, servidores.dFechaBaja, servidores_det_lab.iIdDetalleLab, servidores_det_lab.iIdDependencia, 
servidores_det_lab.iIdClavedPuesto, servidores_det_lab.cCargo, servidores_det_lab.iITipodtrabajador,servidores_det_lab.cNombreTipoTrabajador, 
servidores_det_lab.iIdDepartamento, servidores_det_lab.iIdPuesto, servidores_det_lab.iIdTipo, servidores_det_lab.cStatus, cat_er_depto.cDep_Nombre
FROM servidores
INNER JOIN servidores_det_lab
ON servidores.iIdServidor=servidores_det_lab.iIdServidor
INNER JOIN cat_er_depto
ON cat_er_depto.nIdDepto=servidores_det_lab.iIdDepartamento
WHERE servidores.iIdServidor='$idServ';";
	
$datos=mysql_query($dat_serv,$conexion);	
$row=mysql_fetch_array($datos);

$iIdServidor=$row['iIdServidor'];
$cServidorNombre=$row['cServidorNombre'];
$cServidorApellidoP=$row['cServidorApellidoP'];
$cServidorApellidoM=$row['cServidorApellidoM'];
$cCURP=$row['cCURP'];
$dFechaAlta=$row['dFechaAlta'];
$dFechaBaja=$row['dFechaBaja'];
$cLogin=$row['cLogin'];
$cPassword=$row['cPassword'];
$tObservacion=$row['tObservacion'];
$IdDepen=$row['iIdDependencia'];
$iIdDepartamento=$row['iIdDepartamento'];
$iIdPuesto=$row['iIdPuesto'];
$IdDepen=$row['iIdDependencia'];
$idTipo=$row['iIdTipo'];
$iITipodtrabajador=$row['iITipodtrabajador'];
$cCargo = $row['cCargo'];
$iIdClavedPuesto = $row['iIdClavedPuesto'];
$cNombreTipoTrabajador = $row['cNombreTipoTrabajador'];



$queryDepen="SELECT * FROM cat_dependencia";
$depen=mysql_query($queryDepen,$conexion);

$queryDeptos="SELECT * FROM cat_er_depto where nIdDepen='$IdDepen'";
$deptos=mysql_query($queryDeptos,$conexion);

$queryPuesto="SELECT * FROM cat_er_puestos";
$puesto=mysql_query($queryPuesto,$conexion);

$querytipo = "SELECT * FROM cat_dettipoer WHERE iIdTipo IN (4, 5, 6)";
$type = mysql_query($querytipo,$conexion);

?>

<link href="../script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="script/jscalendar-0.9.6/calendar.js"></script>
<script type="text/javascript" src="script/calendario.js"></script>
<script type="text/javascript" src="js/validaforms.js"></script>
<script type="text/javascript" src="script/validacampos.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>  
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="calendario/css/jquery-ui-1.7.2.custom.css"/>
<script type="text/javascript">
jQuery(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
        'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['es']);
});    

</script>


<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
<link href="script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="captcha/styles.css" rel="stylesheet" type="text/css" />
<!--EMPIEZA CONTENIDO-->
<body onLoad="selectcuenta(this.value)">
<table width="90%" align="center" cellspacing="2" cellpadding="1">
  <tr>
    <td width="892" class="sitios_header">Dar de alta a  servidor <?php echo $cServidorNombre." ".$cServidorApellidoP." ".$cServidorApellidoM ?></td>
    <td width="83" align="center" ><a href="dar_alta.php"><img src="iconos/i_volver.png" alt="" width="47" height="53" border="0" /></a></td>
  </tr>
  <tr>
    <td width="892" class="fila_division2"></td>
    <td align="center">Regresar</td>
  </tr>

</table>
<br>
<form  id="cuentas" name="cuentas" method="post" action="actualiza_alta.php">
<table width="950" border="0" align="center"> 
<tr>
  <td colspan="2" class="fila_subtabla_ci">Detalles del Servidor</td>
  </tr>
<tr>
    <td width="121"></td>
    <td width="838"><label class="hidden">idServidor</label> <input type="hidden" name="IdServidor" id="IdServidor" value="<?php echo $iIdServidor ?>"/></td>
  </tr>
    <tr>
  <td width="121">CURP: </td>
    <td width="838"> <input name="curp" type="text" id="curp" onBlur="valida_curp_cuenta()" onKeyUp="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $cCURP ?>" size="60" maxlength="18" readonly /></td>
  </tr>
    <tr>
    <td width="121">Nombre(s): </td> 
    <td width="838"><input name="nombreServ" type="text" id="nombreServ" value="<?php echo $cServidorNombre ?>" size="40" readonly /></td>
  </tr>
      <tr>
    <td width="121">Apellido Paterno: </td>
    <td width="838"><input name="apellidoP" type="text" id="apellidoP"  value="<?php echo $cServidorApellidoP  ?>" size="40" readonly /></td>
  </tr>
        <tr>
    <td width="121">Apellido Materno: </td>
    <td width="838"><input name="apellidoM" type="text" id="apellidoM"  value="<?php echo $cServidorApellidoM ?>" size="40" readonly /></td>
  </tr>
    <tr>
  <td width="121">Fecha Baja: </td>
    <td width="838"> <input name="fecha_baja" type="text" id="fecha_baja"  value="<?php echo $dFechaBaja ?>" readonly />

  </tr>
  <td width="121">Login: </td>
    <td width="838"> <input name="login" type="text" id="login" value="<?php echo $cLogin ?>" readonly /></td>
  </tr>
  <tr>
   <td width="121">Contrase&ntilde;a: </td>
    <td width="838"> <input name="pass" type="password" id="pass" value="<?php echo $cPassword ?>" readonly /></td>
  </tr>
  </table>
   <br>
<table width="950" border="0" align="center">
    <tr>
      <td colspan="2" class="maxima">Modificar Datos Laborales</td>
    </tr>
    <tr>
    <td width="102">Dependecia/Entidad </td>
    <td align="left">  <?php
	echo "<select disabled class='select' id='cuentas_depen' name='cuentas_depen' onchange='selectcuenta(this.value);' >";
   
    $DepenExist ="SELECT * FROM cat_dependencia WHERE nIdDepen='$IdDepen'";
   
    if ($IdDepen!='N'){
		$query1 =mysql_query($DepenExist);
		while ($rowDepen =mysql_fetch_array($query1 )){
			$DepnNombre =$rowDepen['Dep_Nombre'];
		}
		echo "<option value=$IdDepen> $DepnNombre </option>";
	}else{
		echo"<option value='N'>Seleccione dependencia</option>";}
		 while ($fila = mysql_fetch_array($depen)){
		echo'<option value="'.$fila['nIdDepen'].'"> '.$fila['Dep_Nombre'].' </option>';
		 }
     		echo"</select>";
    ?>
    </td>
  </tr>
  <td width="102">Departamento </td>
  <td>
  <div id="select_cuenta"></div>
  </td>
  <tr>
   <td width="102">puesto </td>
    <td align="left"><?php
   $puestoE ="SELECT * FROM cat_puestos WHERE idPuesto='$iIdPuesto'";
	
    echo "<select class='select' name='idPuesto' id='idPuesto' >";
    if ($iIdPuesto!='N'){
		$query2 =mysql_query($puestoE);
		while ($rowPuesto =mysql_fetch_array($query2 )){
			$puestoN =$rowPuesto['Puesto_Nombre'];
		}
		echo "<option value='$iIdPuesto'> $puestoN </option>";
	}else{
		echo"<option value='N'>Seleccione Puesto</option>";}
		 while ($fila = mysql_fetch_array($puesto)){
		echo'<option value="'.$fila['idPuesto'].'"> '.$fila['Puesto_Nombre'].' </option>';
		 }
     		echo"</select>";
    ?></td>
  </tr>
 
</table>

<table width="950" border="0" align="center">
<div class="error" align="center" ><p><?php 
      if (isset($_GET['error'])=='si')
      {
          echo '<h4>Es necesario ingresar un tipo de alta.</h4>';
      }?></p>
    </div>
    
 
    <input type="hidden" name="iITipodtrabajador" id="iITipodtrabajador" value="<?php echo $iITipodtrabajador; ?>" />
    <input type="hidden" name="cCargo" id="cCargo" value="<?php echo $cCargo; ?>" />
     <input type="hidden" name="iIdClavedPuesto" id="iIdClavedPuesto" value="<?php echo $iIdClavedPuesto; ?>" />
     <input type="hidden" name="cNombreTipoTrabajador" id="cNombreTipoTrabajador" value = "<?php echo $cNombreTipoTrabajador; ?>" />
    
  
    <tr>
      <td colspan="2" class="maxima">Dar de Alta</td>
    </tr>
     <tr>
   
  <td width="102">Tipo de Alta</td>
  <td align="left"><?php
   //$cattipo ="SELECT * FROM cat_dettipoer WHERE iIdTipo='$idTipo'";
  
    echo "<select class='select' name='iIdTipo' id='iIdTipo' >";
    /*if ($idTipo!='N'){
    $query2 =mysql_query($cattipo);
    while ($rowTipo =mysql_fetch_array($query2 )){
      $tipoN =$rowTipo['vDescripcion'];
    }
    echo "<option value='$idTipo'> $tipoN </option>";
  }else{*/
    echo"<option value='N'>------Seleccione Tipo Alta------</option>";
     while ($fila = mysql_fetch_array($type)){
    echo'<option value="'.$fila['iIdTipo'].'"> '.$fila['vDescripcion'].' </option>';
     }
        echo"</select>";
    ?></td> 
  </tr>
  <tr>
  <tr>
 
    <td width="121">Fecha Alta: </td>
    <td width="838"> <input type="text" name="fecha_alta" id="fecha_alta" /></td>
    <!--<span style="color:#999999;">Ejemplo: <?php echo date('Y-m-d')?></span></td>-->
    
  </tr>
    <td>Observaciones </td>
    <td><p>
      <label for="observacion"></label>
      <textarea name="observacion" cols="65" rows="5" id="observacion"></textarea>
    </p></td>
  </tr>
  <tr>
  <td><input type="submit" name="enviar_test" id="enviar_test" value="DAR DE ALTA" class="btn btn-warning"></td>
<!-- <td><input class="boton_save" type="submit" id="modifica" method="post" name="modifica"  action="ACTUALIZAR_DATOS.php">
    <span class="noticia_header">Guardar Cambios</span></td>-->
  </tr>

</form>

</body>

<!--TERMINA CONTENIDO-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11177675-1");
pageTracker._trackPageview();
} catch(err) {}
</script>




<?php webFooter(); ?>