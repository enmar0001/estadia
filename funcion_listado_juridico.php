<script type="text/javascript" src="js/validaforms.js"></script>
<script type="text/javascript" src="script/validacampos.js"></script>
<script type="text/javascript" src="script/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="script/datatables/tablas.js"></script>

<link href="../script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
<link href="script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="captcha/styles.css" rel="stylesheet" type="text/css" />

<link href="css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.foundation.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="funcion_listado_juridico.js"></script>
 
<?php
include('conexion.php');

//recibe parametros desde el js
$depen=$_POST['IdDepen'];
$depto=$_POST['IdDepto'];
$tipo=$_POST['idStatus'];

	//arma usuarios

	$IDepen = $depen;
	switch($IDepen){
	case "N":
	$queryDepen="";
	break;
	case $IDepen:
	$queryDepen=" AND servidores_det_lab.iIdDependencia='$IDepen' ";
	break;
	}
	
	$IDdepto=$depto;
	switch($IDdepto){
	case "N":
	$queryDepto="";
	break;
	case $IDdepto:
	$queryDepto=" AND servidores_det_lab.iIdDepartamento='$IDdepto' ";
	break;
	}

	$IDStatus=$tipo;
	switch($IDStatus){
	case "N":
	$queryStatus="";
	break;
	case $IDStatus:
	$queryStatus=" AND servidores_det_lab.iIdTipo = '$IDStatus' ";
	break;
	}
	
/*$queryLista = "SELECT servidores.iIdServidor, servidores.cServidorNombre, servidores.cServidorApellidoP, servidores.cCURP, servidores.cServidorApellidoM, 
cat_dependencia.Dep_Nombre, cat_er_depto.cDep_Nombre
FROM servidores, servidores_det_lab, cat_dependencia, cat_er_depto
WHERE servidores.iIdServidor = servidores_det_lab.iIdServidor
AND servidores_det_lab.iIdDependencia = cat_dependencia.nIdDepen
AND servidores_det_lab.iIdDepartamento = cat_er_depto.nIdDepto
$queryTipo";*/

//Consulta para la importación a excel.
$queryLista = "SELECT servidores.iIdServidor, servidores.cServidorNombre, servidores.cServidorApellidoP, servidores.cServidorApellidoM, servidores.cCURP, cat_dependencia.Dep_Nombre, cat_er_depto.cDep_Nombre, servidores_det_lab.cStatus, servidores_det_lab.dFechaIngreso, cat_dettipoer.vDescripcion FROM servidores, servidores_det_lab, cat_dependencia, cat_er_depto, cat_dettipoer WHERE servidores.iIdServidor = servidores_det_lab.iIdServidor AND servidores_det_lab.iIdDependencia = cat_dependencia.nIdDepen AND servidores_det_lab.iIdDepartamento = cat_er_depto.nIdDepto AND servidores_det_lab.cPermisos = 'U' AND servidores_det_lab.iHistorico = 1 AND servidores_det_lab.iIdTipo = cat_dettipoer.iIdTipo $queryDepen $queryDepto $queryStatus ORDER BY servidores.iIdServidor DESC";
	
?>

<table id="jur1" width="900">
<tr>
<td width="700">
<h3>Lista de Sujetos Obligados</h3>
</td>
<td width="200" align="center">
<a href="excelso.php?query=<?php echo $queryLista; ?>"><img src="iconos/btn_reporteexcel.png" width="127" height="29" /></a>
</td>
</tr>
</table>

<table width="950" height="43" border="1" bordercolor="#8FC133" align="center">
<thead>
                            <tr class="fila_subenc_tabla" align="center"> 
                              <td width="130" class="maxima" ><strong>CURP</strong></td>
                              <td width="203" class="maxima" ><strong>Nombre</strong></td>
                              <td width="283" class="maxima" ><strong>Dependencia/Entidad</strong></td>
                              <td width="283" class="maxima" ><strong>Departamento</strong></td>
                              <td width="283" class="maxima" ><strong>Estatus</strong></td>
                              <td width="283" class="maxima" ><strong>Motivo</strong></td>
                              <td width="283" class="maxima" ><strong>Fecha Alta</strong></td>
                              <td width="86" class="maxima" ><strong>Movimientos</strong></td>
                              <!--<td width="87" class="maxima" >Entrega</td>-->
                              <!--<td width="121" class="maxima" >Cambiar estatus</td>-->
                              <!--<td width="81"><strong>Fecha Captura</strong></td>-->
                            </tr>
</thead>

<tbody>                            <tr align="center">
 <?php
 
 //Se pone de nuevo la consulta para la lectura de los datos en la tabla
$queryLista = "SELECT servidores.iIdServidor, servidores.cServidorNombre, servidores.cServidorApellidoP, servidores.cServidorApellidoM, 
servidores.cCURP, cat_dependencia.Dep_Nombre, cat_er_depto.cDep_Nombre, servidores_det_lab.cStatus, 
servidores_det_lab.dFechaIngreso, cat_dettipoer.vDescripcion
FROM servidores, servidores_det_lab, cat_dependencia, cat_er_depto, cat_dettipoer
WHERE servidores.iIdServidor = servidores_det_lab.iIdServidor
AND servidores_det_lab.iIdDependencia = cat_dependencia.nIdDepen
AND servidores_det_lab.iIdDepartamento = cat_er_depto.nIdDepto
AND servidores_det_lab.cPermisos = 'U'
AND servidores_det_lab.iHistorico = 1
AND servidores_det_lab.iIdTipo = cat_dettipoer.iIdTipo
$queryDepen
$queryDepto
$queryStatus
ORDER BY servidores.iIdServidor DESC";
	
	$lista=mysql_query($queryLista,$conexion);
	  while($list=mysql_fetch_array($lista))
	
		  
	    echo  
		'<td align="center">'.$list['cCURP'].'</td>
	     <td align="center">'.$list['cServidorNombre'].' '.$list['cServidorApellidoP'].' '.$list['cServidorApellidoM'].'</td>
	     <td align="center">'.utf8_encode($list['Dep_Nombre']).'</td>
		 <td align="center">'.utf8_encode($list['cDep_Nombre']).'</td>
		 <td align="center">'.$list['cStatus'].'</td>
		 <td align="center">'.$list['vDescripcion'].'</td>
		 <td align="center">'.$list['dFechaIngreso'].'</td>
		  <td align="center"><a href="lista_sujetosob_juridico_detalle_mov.php?id_so='.$list['iIdServidor'].'"><img src="iconos/historial_mov.png" width="30" height="30" /></a></td> 
		  <!--<td align="center"><a href="Solicitud_de_intervencion.php?id='.$list['iIdServidor'].'"><img src="iconos/n_entrega.gif" width="30" height="30" /></a></td>--> 
		  <!--<td align="center"><a href="lista_sujetosob_juridico_detalle_mov.php?id_so='.$list['iIdServidor'].'"><img src="iconos/historial_mov.png" width="30" height="30" /></a></td>--> 
         </tr>';
?>
  </tr>
  </tbody>
</table>

<br />
<br />
</body>