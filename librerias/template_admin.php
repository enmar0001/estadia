<?php session_start();

function candado_admin(){
    if($_SESSION['Id_Usu']=='' || $_SESSION['Tipo_User'] != 'I'){
    header('location:error.php');
    }
  }

function webCabezal($titulo){ 
$titulo='Administración';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <title>Administración</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
                                                 <!-- ../../stylesheets/theme.css -->
    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">

    <script src="../lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="">
	<script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $(".demo-cancel-click").click(function(){return false;});
        });
    </script>
<!--termina popup-->
<?php	} ?>

</head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>Bienvenido <?php echo $nombre=$_SESSION['nomserv'].' '.$_SESSION['apellidoP'].' '.$_SESSION['apellidoM']; ?> <!-- AQUI VA EL USUARIO -->
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="saliradmin.php">Salir</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.php"><span class="first"><img src="../iconos/logo_admin.png" width="138" height="44" /></span> <span class="second"></span></a>
        </div>
    </div>
    


    
    <div class="sidebar-nav">
        <!--<form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>-->

        <a href="#dashboard-menu" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>Usuarios<i class="icon-chevron-up"></i></a>
        <ul id="dashboard-menu" class="nav nav-list collapse in">
            <li ><a href="cat_coordinadores_de_enlace.php">Coordinadores de Enlace </a></li>
            <li ><a href="cat_sujetos_obligados_admin.php">Sujetos Obligados</a></li>
                       
        </ul>

        <a href="#accounts-menu" class="nav-header" data-toggle="collapse"><i class="icon-briefcase"></i>Catalogos<i class="icon-chevron-up"></i></a>
        <ul id="accounts-menu" class="nav nav-list collapse in">
            <li ><a href="cat_dependencias.php">Dependencias</a></li>
        </ul>
		
		
    </div>
    

    
    <div class="content">
        
        <div class="header">
            <div class="stats">

</div>

            <h1 class="page-title">Panel Administrador</h1>
        </div>
        
             
        <div class="container-fluid">
            <div class="row-fluid">
                    


                        <?php	function webFooter(){ ?>
  
                    
                    <footer>
                        <hr>
                        <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                        <p class="pull-right"> &copy; 2016 <a href="http://www.contraloria.yucatan.gob.mx/" target="_blank">Secretaría De La Contraloría General</a><a href="#" target="_blank"></a></p>
                        

                        <p> <a href="#" target="_blank"></a></p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    
    
  </body>
</html>
<?php	} ?>

<?php	function webFooterPopup(){ ?>
	</body>
	</html>
<?php }?>

