<?php
//Panel jurídico
include('conexion.php');
include("librerias/template_juridico.php");

candado();
webCabezal("Manejador de Coordinadores");



$nombre=$_SESSION['nomserv'].' '.$_SESSION['apellidoP'].' '.$_SESSION['apellidoM'];
$id=$_SESSION['idServidor'];

$dat_serv="SELECT * FROM servidores_det_lab WHERE iIdServidor='$id';";	
$datos=mysql_query($dat_serv,$conexion);	
$row=mysql_fetch_array($datos);

$iIdDetalleLab=$row['iIdDetalleLab'];
$iIdServidor=$row['iIdServidor'];
$IdDepen=$row['iIdDependencia'];
$iIdDepartamento=$row['iIdDepartamento'];
$iIdPuesto=$row['iIdPuesto'];
$IdSector=$row['iIdSector'];
$dFechaIngreso=$row['dFechaIngreso'];
$dFechaBaja=$row['dFechaBaja'];
$cTelOficina=$row['cTelOficina'];
$cCorreoOficina=$row['cCorreoOficina'];
$cPermisos=$row['cPermisos'];
$iIdTipo=$row['iIdTipo'];
$cStatus=$row['cStatus'];
$tObservacion=$row['tObservacion'];

?>

<script type="text/javascript" src="js/validaforms.js"></script>
<script type="text/javascript" src="script/validacampos.js"></script>
<script type="text/javascript" src="funcion_listado_juridico.js"></script>
<script type="text/javascript" src="script/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="script/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="script/datatables/tablas.js"></script>

<link href="../script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
<link href="script/estilos_sitio.css" rel="stylesheet" type="text/css" />
<link href="captcha/styles.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.foundation.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/datatables/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />


<body onLoad="buscasujeto()">

<!--Scrip para que el combo de departamentos cambie dependiendo de la dependencia que se elija.-->
<script type="text/javascript">
            $(document).ready(function(){
                $("#div_listado").html("<img src='images/loading.gif' />");
                $('#IdDepen').change(function(){
                    var id=$('#IdDepen').val();
                    $("#div_listado").html("<img src='images/loading.gif' />");
                    $('#depas').load('ajax_depa.php?id='+id);
                });

              $('#IdDepto').change(function(){
                    $("#div_listado").html("<img src='images/loading.gif' />");
                });

              $('#idStatus').change(function(){
                    $("#div_listado").html("<img src='images/loading.gif' />");
                });    
            });
        </script>

<table width="950" border="0"  align="center" >
  <tr>
    <td width="80" rowspan="2">
    <img src="iconos/<?php echo $_SESSION['iconuser'] ?>" width="76" height="80">
    </td>
    <td width="549" rowspan="2" class="sitios_header"><?php echo $nombre ?><br><br>
    <input name="dependencia" type="hidden" class="Caja" value="<?php 
						   
	$queryDependencia="SELECT * FROM cat_dependencia where nIdDepen='$IdDepen'";
	$dependencia=mysql_query($queryDependencia,$conexion);
	  while($dep=mysql_fetch_array($dependencia)){
	    echo $dep['Dep_Nombre']; $depa=$dep['Dep_Nombre']; $iddepa=$dep['nIdDepen'];
	  }?>" size="90" readonly />
      
      <span class="fila_division2"><?php echo $depa?></span>
      
      </td>
    <td width="84" align="center"><a href="perfil_juridico.php"><img src="iconos/n_edit.png" alt="" width="70" height="65" border="0" /></a></td>
    <!--<td width="70" align="center"><a href="add_cuenta.php"><img src="iconos/n_addusuario.jpg" alt="" width="70" height="65" border="0" /></a></td>-->
    <td width="71" align="center"><a href="salir.php"><img src="iconos/n_exit.png" alt="" width="65" height="65" border="0" /></a></td>
  </tr>
  <tr>
    <td align="center">Editar mis datos</td>
    <!--<td align="center">Agregar</td>-->
    <td align="center">Salir</td>
  </tr>
</table>

<table width="950" border="0" align="center">
  <tr>
  <td>
  <input type="hidden" name="id_depen" id="id_depen" value="<?php echo $IdDepen ?>" />
  </td>
  </tr>
  <tr>
  <td>Filtrar por:
  </td></tr>
  
  <tr>
  <td>
<strong>Dependencia/Entidad:</strong>
<div id="ent">
        <?php 
						   
	$queryDependencia="SELECT * FROM cat_dependencia ORDER BY Dep_Nombre ASC;";
	$dep=mysql_query($queryDependencia,$conexion);?>
                            <select name="IdDepen" id="IdDepen" class="select" style="width:220px" onChange="buscasujeto()">
                              <option value="N">---Seleccione entidad/dependencia---</option>
                              <?php while ($fila = mysql_fetch_array($dep)){
					       ?>
                              <option value="<?php echo $fila["nIdDepen"]?>"><?php echo $fila["Dep_Nombre"] ?></option>
                              <?php  } ?>
                            </select>
                            </div>
  </td>
  </tr>
  
  <tr>
  <td>
<strong>Departamento:</strong>
        <div id="depas">
                            <select disabled name="IdDepto" id="IdDepto" class="select" style="width:220px" onChange="buscasujeto()">
                              <option value="N">---ELEGIR DEPENDENCIA PRIMERO---</option>
                              </select>
                              </div>
  </td>
  </tr>

  <tr>
  <td>
<strong>Estatus:</strong>
<div id="stat">
        <?php 
						   
	$queryS="SELECT * FROM cat_dettipoer ORDER BY vDescripcion ASC;";
	$Stat=mysql_query($queryS,$conexion);?>
                            <select name="idStatus" id="idStatus" class="select" style="width:220px;" onChange="buscasujeto()">
                              <option value="N">---Seleccione Estatus---</option>
                              <?php while ($fila = mysql_fetch_array($Stat)){
					       ?>
                              <option value="<?php echo $fila["iIdTipo"]?>"><?php echo $fila["vDescripcion"] ?></option>
                              <?php  } ?>
                            </select>
                            </div>
  </td>
  </tr>

</table>

 <div id="div_listado"></div>

</body>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11177675-1");
pageTracker._trackPageview();
} catch(err) {}
</script>




<?php webFooter(); ?>