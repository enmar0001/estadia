

<?php
require_once("dompdf/dompdf_config.inc.php");


$html='<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="script/estilo_tabla.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="541">
    <tr>
        <td width="154"><img src="iconos/SECRETARIA DE LA CONTRALORIA GENERAL.jpg" width="146"/></td>
        <td width="375" align="center"><p class="fila_subencabezado_menu1">SECRETARÍA DE LA CONTRALORÍA GENERAL</p>
          <p class="fila_subencabezado_menu1">DIRECCÍON DE AUDITORIA AL SECTOR CENTRALIZADO</p>
        <p class="fila_subencabezado_menu1">SISTEMA DE CONTROL INTERNO INSTITUCIONAL</p>
        <p class="fila_brown"> PLAN DE ACCIÓN</p></td>
    </tr>
</table>
<table width="541" border="1" class="tab_tabla_nar_1">
      <tr>
          <td align="center" colspan="3" class="fila_resultados">IDENTIFICACIÓN DE LA DEBILIDAD EN EL SISTEMA DE CONTROL INTERNO INSTITUCIONAL</td>
      </tr>
      <tr>
        <td width="126">Código:</td>
        <td colspan="2" ><strong>Origen:</strong> (favor de marcar con una X)</td>
      </tr>
      <tr>
          <td rowspan="6">&nbsp;</td>
          <td width="21" >&nbsp;</td>
          <td width="372" >Auditoria de Control Interno</td>
      </tr>
      <tr>
          <td >&nbsp;</td>
          <td >Evaluación Interna</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td >Quejas</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td >Operación Diaria (Desempeño del proceso)</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td >Incumplimiento de Indicadores de desempeño y/o metas</td>
      </tr>
  <tr>
        <td >&nbsp;</td>
    <td >Acuerdos del Comite de Control Interno</td>
  </tr>
</table>
<table width="541" height="203" border="1" class="tab_tabla_nar_1">
  <tr>
    <td width="349"  valign="top">Descripción de la debilidad: </td>
    <td width="176"  valign="top">Fecha de indentificación:</td>
  </tr>
  <tr>
    <td valign="top">Nombre y área de quien detectó:</td>
    <td valign="top">Fecha de implementación:</td>
  </tr>
  <tr>
    <td valign="top">Responsable de atender:</td>
    <td valign="top">Clave de Proceso:</td>
  </tr>
  <tr>
    <td valign="top">Análisis de Causa:</td>
    <td valign="top">Nombre del Procedimiento:</td>
  </tr>
</table>

<!--otra tabla-->
<table width="541" height="128"  border="1" class="tab_tabla_nar_1">
    <tr>
    	<td height="23" colspan="2" align="center" class="fila_resultados"><strong>ACCIONES PREVENTIVAS Y CORRECTIVAS </strong></td>
    </tr>
    <tr>
      <td rowspan="2"  valign="top"> Acciones preventivas:</td>
        <td  valign="top"> Acciones correctivas inmediatas:</td>
    </tr>
    <tr>
    	<td valign="top">Acciones correctivas permanentes:</td>
    </tr>
</table>
<table width="541" height="60" border="2" class="tab_tabla_nar_1">
  <tr>
      <td width="129" height="52"  align="center" valign="top">Elaboró</td>
      <td width="214"  align="center" valign="top"><p>Autorizó</p>
      <p>C.P. Juan José Vázquez Pereira</p></td>
    <td width="174"  align="center" valign="top"><p>Enterado
    </p>
    <p>C.P. Rosana Rodríguez</p></td>
</tr>
</table>
<table width="541" border="2" >
    <tr>
    	<td  align="center" class="fila_resultados"><strong>SEGUIMIENTO </strong></td>
    </tr>
</table>
<table width="541"  border="1" class="tab_tabla_nar_1">
    <tr>
    <td colspan="2" valign="top">Fecha de seguimiento:</td>
    </tr>
    <tr>
    <td colspan="2" valign="top">Se consideran efectivas las acciones? </td>
    </tr>
    <tr>
        <td width="119">SI<input type="checkbox" id="si"><label>se procede al cierre</label></td>  
      <td width="231">NO<input type="checkbox" id="si">
      <label>Fecha de reprogramación de cierre:</label></td>
    </tr>
</table>
<table width="541" height="67" border="1" class="tab_tabla_nar_1">
  <tr>
        <td width="312" height="61"  valign="top">Observaciones:</td>
        <td width="213" align="center"  valign="top"><p>Verificó</p>
        <p>Pedro Caceres </p></td>
  </tr>
</table>
<table width="541" border="1" class="tab_tabla_nar_1">
    <tr>
  	  <td align="center" class="fila_resultados"><strong>CIERRE </strong></td>
    </tr>
</table>
<table width="541" height="80" border="1" class="tab_tabla_nar_1">
<tr>
      <td height="19" colspan="3" >Fecha de Cierre:</td>
    </tr>
    <tr>
      <td width="173" height="53" align="center" valign="top">Enterado</td>
      <td width="172" align="center" valign="top">Enterado<p>C.P. Juan José Vázquez Pereira</p></td> 
      <td width="163" align="center" valign="top">Enterado<p>C.P. Rosana Rodríguez</p></td>
    </tr>
</table>
</body>
</html>';

$dompdf = new DOMPDF();
$dompdf->set_paper("letter");
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream("sample.pdf")

?>